package sunil.thorangs.com.firebase;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    private final String TAG = "ftest";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnSave = findViewById(R.id.btnAdd);
        Button btnLoad = findViewById(R.id.btnLoad);
        final TextView textMsg = findViewById(R.id.tvMsg);
        final TextView textMsg2 = findViewById(R.id.tvMsg2);

        final EditText etId = findViewById(R.id.etId);
        final EditText etName = findViewById(R.id.etName);

        final FirebaseFirestore db = FirebaseFirestore.getInstance();

        btnSave.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Map<String, Object> info = new HashMap<>();
                info.put("id", etId.getText().toString());
                info.put("name", etName.getText().toString());

                db.collection("info").document(etId.getText().toString())
                        .set(info)
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                textMsg.setText("User Info successfully written!");
                                Log.d(TAG, "User Info successfully written!");
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                textMsg.setText("Error writing User Info");
                                Log.w(TAG, "Error writing User Info", e);
                            }
                        });



            }
        });

        btnLoad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                db.collection("info")
                        .whereEqualTo("id", etId.getText().toString())
                        .get()
                        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> task) {

                                if (task.isSuccessful()) {
                                    if(task.getResult().isEmpty()){
                                        Log.d(TAG, "record not found!!!");
                                        textMsg.setText("empty data!!, record not found");
                                    }
                                    for (DocumentSnapshot document : task.getResult()) {
                                        Log.d(TAG, document.getId() + " => " + document.getData());
                                        textMsg.setText("success, date : " + document.getId() + " => " + document.getData());
                                    }
                                } else {
                                    textMsg.setText("Error writing document");
                                    Log.w(TAG, "Error getting documents.", task.getException());
                                }
                            }


                        });
            }
        });


        final DocumentReference docRef = db.collection("info").document("1");//etId.getText().toString());
        docRef.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot snapshot,
                                @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    textMsg2.setText("Listen failed.");
                    Log.w(TAG, "Listen failed.", e);
                    return;
                }

                if (snapshot != null && snapshot.exists()) {
                    textMsg2.setText("Current data: " + snapshot.getData());
                    Log.d(TAG, "Current data: " + snapshot.getData());
                } else {
                    textMsg2.setText("Current data: null");
                    Log.d(TAG, "Current data: null");
                }
            }
        });



    }
}
